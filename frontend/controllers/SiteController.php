<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Record;

class SiteController extends Controller
{

	public function behaviors()
	{
		return [];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex()
	{
		$model = new Record();

		if (Yii::$app->request->post() && $model->validate()) {
			$model->save();

			$model->name = Yii::$app->request->post()['Record']['name'];
			$model->email = Yii::$app->request->post()['Record']['email'];
			$model->subject = Yii::$app->request->post()['Record']['subject'];
			$model->message = Yii::$app->request->post()['Record']['message'];

			$model->save();

			return $this->render('success', [
				'name' => $model->name
			]);
		}

		return $this->render('index', [
			'model' => $model,
		]);
	}

}

<?php

use yii\helpers\Html;

$this->title = 'Успех';

?>
<div class="site-error">

    <div class="alert alert-success">
        <h1>
        		 Поздравляем 
            <?= $name ?>, вы успешно записанны к нам. Мы уже выехали за вами! :)
        </h1>
    </div>

</div>

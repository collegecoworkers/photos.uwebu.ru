<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use developit\captcha\Captcha;

$this->title = Yii::t('app', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">

    <div class="row">
    <div class="col-lg-6 col-lg-offset-3">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= Html::submitButton(Yii::t('app', 'Войти'), ['class' => 'btn btn-login', 'name' => 'login-button']) ?>
            <?= Html::a("Регистрация", ['site/signup']); ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

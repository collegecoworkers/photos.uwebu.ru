<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

$this->title = Yii::t('app', 'Заявки');
$this->params['breadcrumbs'][] = $this->title;
$status = '';
?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">
<div class="contact-index">
	<div class="fa-br"></div>
	<br>
	<?php
	$dataProvider = new ActiveDataProvider([
		'query' => $model,
		'pagination' => [
		 'pageSize' => 20,
		],
	]);
	echo GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => "{items}\n{pager}",
		'columns' => [
			// ['class' => 'yii\grid\SerialColumn'],
			'id',
			'name',
			'email',
			'subject',
			'message',
			[
				'label' => 'Действие',
				'format' => 'raw',
				'value' => function($dataProvider){
					return Html::a("Удалить", ['site/delete', 'id' => $dataProvider->id]);
				},
			],
		],
	]);
	?>

</div>

		</div>
	</div>
</div>

<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use backend\models\Signup;
use backend\models\Login;
use common\models\Record;

class SiteController extends Controller
{

	public function behaviors(){
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['signup', 'login', 'error'],
						'allow' => true,
					],
					[
						'actions' => [
							'logout',
							'delete',
							'index',
							'update',
						],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actions(){
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex(){
		$model = Record::find()->orderBy('id desc');
		return $this->render('index', ['model' => $model]);
	}

	public function actionDelete($id) {
		Record::findOne($id)->delete();
		return $this->redirect(['index']);
	}

	public function actionSignup(){
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Signup();

		if(isset($_POST['Signup'])){
			$model->attributes = Yii::$app->request->post('Signup');
			if($model->validate() && $model->signup()){
				return $this->redirect(['index']);
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}

	public function actionLogin(){
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Login();

		if( Yii::$app->request->post('Login')) {
			$model->attributes = Yii::$app->request->post('Login');
			if($model->validate()) {
				$user = $model->getUser();

				Yii::$app->user->login($user);

				return $this->goHome();
			}
		}

		return $this->render('login', [
			'model' => $model,
		]);
	}

	public function actionLogout(){
		Yii::$app->user->logout();
		return $this->goHome();
	}
}

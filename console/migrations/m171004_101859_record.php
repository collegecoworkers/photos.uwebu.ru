<?php

use yii\db\Migration;

class m171004_101859_record extends Migration
{
    public function safeUp()
    {
        $this->createTable('record', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'email' => $this->string(),
            'subject' => $this->string(),
            'message' => $this->text(),
        ]);
    }

    public function safeDown()
    {
        return false;
    }
}
